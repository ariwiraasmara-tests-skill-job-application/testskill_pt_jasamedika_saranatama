<?php
namespace Tests\Unit;

use Tests\TestCase;
use Tests\Unit\Factory;
// use PHPUnit\Framework\TestCase;
use App\Models\Kelurahan;
use App\Repositories\KelurahanRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

class KelurahanTest extends TestCase {

    use RefreshDatabase;

    /**
     * A basic unit test example.
     */
    public function test_example(): void{
        $this->assertTrue(true);
    }

    public function testGetAllKelurahan() {
        // $kelurahan = factory(Kelurahan::class, 3)->create();
        $kelurahan = Kelurahan::factory()->count(1)->create();
        $kelurahanRepository = new KelurahanRepository(new Kelurahan());
        $result = $kelurahanRepository->allData();
        $this->assertEquals($kelurahan->toArray(), $result->toArray());
    }

    public function testCreateNewKelurahan() {
        $data = [
            'nama_kelurahan' => 'Mekarsari',
            'nama_kecamatan' => 'Cimanggis',
            'nama_kota' => 'Depok',
        ];

        $kelurahanRepository = new KelurahanRepository(new Kelurahan());
        $result = $kelurahanRepository->store($data);

        $this->assertInstanceOf(Kelurahan::class, $result);
        $this->assertDatabaseHas('kelurahan', $data);
    }

    public function testUpdateExistingKelurahan() {
        // $kelurahan = factory(Kelurahan::class)->create();
        $kelurahan = Kelurahan::factory()->count(1)->create()->first()->id;;

        $data = [
            'id' => $kelurahan->id,
            'nama_kelurahan' => 'Mekarsari',
            'nama_kecamatan' => 'Cimanggis',
            'nama_kota' => 'Depok',
        ];

        // $kelurahanRepository = new KelurahanRepository($kelurahan);
        $kelurahanRepository = new KelurahanRepository(new Kelurahan());

        $result = $kelurahanRepository->updateAll($data, $kelurahan->id);

        $this->assertInstanceOf(Kelurahan::class, $result);
        $this->assertDatabaseHas('kelurahan', $data);
    }

    public function testUpdateNonExistingKelurahan() {
        $kelurahanRepository = new KelurahanRepository(new Kelurahan());
        $result = $kelurahanRepository->updateAll(1, []);
        $this->assertFalse($result);
    }

    public function testDeleteExistingKelurahan() {
        // $kelurahan = factory(Kelurahan::class)->create();
        $kelurahan = Kelurahan::factory()->count(1)->create();

        // $kelurahanRepository = new KelurahanRepository($kelurahan);
        $kelurahanRepository = new KelurahanRepository(new Kelurahan());

        $result = $kelurahanRepository->delete($kelurahan->id);

        $this->assertTrue($result);
        $this->assertDatabaseMissing('kelurahans', $kelurahan->toArray());
    }
}
