<?php
namespace Tests\Unit;

use Tests\TestCase;
use Tests\Unit\Factory;
// use PHPUnit\Framework\TestCase;
use App\Models\Pasien;
use App\Repositories\PasienRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PasienTest extends TestCase {

    use RefreshDatabase;

    /**
     * A basic unit test example.
     */
    public function test_example(): void {
        $this->assertTrue(true);
    }

    public function testGetAllPasien() {
        // $kelurahan = factory(Kelurahan::class, 3)->create();
        $pasien = Pasien::factory()->count(1)->create();
        $pasienRepository = new PasienRepository(new Pasien());
        $result = $pasienRepository->allData();
        $this->assertEquals($pasien->toArray(), $result->toArray());
    }

    public function testCreateNewPasien() {
        $data = [
            'id_pasien'     => '2303000001',
            'nama_pasien'   => 'Andi',
            'alamat'        => 'lorem upsum',
            'no_telepon'    => '08520',
            'rt_rw'         => '01/02',
            'kelurahan'     => 1,
            'tgl_lahir'     => date('Y-m-d'),
            'jenis_kelamin' => 'L',
        ];

        $pasienRepository = new PasienRepository(new Pasien());
        $result = $pasienRepository->store($data);

        $this->assertInstanceOf(Pasien::class, $result);
        $this->assertDatabaseHas('pasien', $data);
    }

    public function testUpdateExistingPasien() {
        // $kelurahan = factory(Kelurahan::class)->create();
        $pasien = Pasien::factory()->count(1)->create()->first()->id;;

        $data = [
            'id' => $pasien->id,
            'nama_kelurahan' => 'Mekarsari',
            'nama_kecamatan' => 'Cimanggis',
            'nama_kota' => 'Depok',
        ];

        // $kelurahanRepository = new KelurahanRepository($kelurahan);
        $pasienRepository = new PasienRepository(new Pasien());

        $result = $pasienRepository->update($data, $pasien->id);

        $this->assertInstanceOf(Pasien::class, $result);
        $this->assertDatabaseHas('pasien', $data);
    }

    public function testUpdateNonExistingPasien() {
        $pasienRepository = new PasienRepository(new Pasien());
        $result = $pasienRepository->update(1, []);
        $this->assertFalse($result);
    }

    public function testDeleteExistingPasien() {
        // $kelurahan = factory(Kelurahan::class)->create();
        $pasien = Pasien::factory()->count(1)->create();

        // $kelurahanRepository = new KelurahanRepository($kelurahan);
        $pasienRepository = new PasienRepository(new Pasien());

        $result = $pasienRepository->delete($pasien->id);

        $this->assertTrue($result);
        $this->assertDatabaseMissing('pasien', $pasien->toArray());
    }
}
