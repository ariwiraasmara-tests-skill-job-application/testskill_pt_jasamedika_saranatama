<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title inertia>{{ config('app.name', 'Laravel') }}</title>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="My Cloud Player | Your Own Media Music Player Online" />
        <meta content="IE=edge" http-equiv="x-ua-compatible">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no" >
        <meta name="apple-mobile-web-app-capable" content="yes" >
        <meta name="apple-touch-fullscreen" content="yes" >

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/additional.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/custel.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.min.css">

        <!-- Scripts -->
        @routes
        @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
        @inertiaHead

        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    </head>
    <body class="font-sans antialiased">
        @inertia

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.all.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.all.min.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                // Functions to open and close a modal
                function openModal($el) {
                $el.classList.add('is-active');
                }

                function closeModal($el) {
                $el.classList.remove('is-active');
                }

                function closeAllModals() {
                (document.querySelectorAll('.modal') || []).forEach(($modal) => {
                    closeModal($modal);
                });
                }

                // Add a click event on buttons to open a specific modal
                (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
                const modal = $trigger.dataset.target;
                const $target = document.getElementById(modal);

                $trigger.addEventListener('click', () => {
                    openModal($target);
                });
                });

                // Add a click event on various child elements to close the parent modal
                (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
                const $target = $close.closest('.modal');

                $close.addEventListener('click', () => {
                    closeModal($target);
                });
                });

                // Add a keyboard event to close all modals
                document.addEventListener('keydown', (event) => {
                const e = event || window.event;

                if (e.keyCode === 27) { // Escape key
                    closeAllModals();
                }
                });
            });
        </script>
    </body>
</html>
