<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pasien>
 */
class PasienFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array {
        return [
            //
            'id_pasien'     => date('ym').str_pad(1, 6, "0", STR_PAD_LEFT),
            'nama_pasien'   => fake()->name(),
            'alamat'        => Str::random(10),
            'no_telepon'    => Integer::random(15),
            'rt_rw'         => '01/02',
            'kelurahan'     => 1,
            'tgl_lahir'     => date('Y-m-d'),
            'jenis_kelamin' => 'L',
        ];
    }
}
