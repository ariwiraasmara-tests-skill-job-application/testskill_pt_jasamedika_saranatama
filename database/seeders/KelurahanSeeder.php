<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KelurahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('kelurahan')->insert([
            'id'             => 1,
            'nama_kelurahan' => 'Cipocok Jaya',
            'nama_kecamatan' => 'Cipocok Jaya',
            'nama_kota'      => 'Serang',
        ]);

        DB::table('kelurahan')->insert([
            'id'             => 2,
            'nama_kelurahan' => 'Curug',
            'nama_kecamatan' => 'Curug',
            'nama_kota'      => 'Serang',
        ]);

        DB::table('kelurahan')->insert([
            'id'             => 3,
            'nama_kelurahan' => 'Kasemen',
            'nama_kecamatan' => 'Kasemen',
            'nama_kota'      => 'Serang',
        ]);

        DB::table('kelurahan')->insert([
            'id'             => 4,
            'nama_kelurahan' => 'Serang',
            'nama_kecamatan' => 'Serang',
            'nama_kota'      => 'Serang',
        ]);

        DB::table('kelurahan')->insert([
            'id'             => 5,
            'nama_kelurahan' => 'Taktakan',
            'nama_kecamatan' => 'Taktakan',
            'nama_kota'      => 'Serang',
        ]);

        DB::table('kelurahan')->insert([
            'id'             => 6,
            'nama_kelurahan' => 'Walantaka',
            'nama_kecamatan' => 'Walantaka',
            'nama_kota'      => 'Serang',
        ]);
    }
}
