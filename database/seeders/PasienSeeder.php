<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //

        $counter = 1;
        DB::table('pasien')->insert([
            'id_pasien'     => date('ym').'00000'.$counter,
            'nama_pasien'   => 'Andi',
            'alamat'        => 'lorem ipsum',
            'no_telepon'    => '08123',
            'rt_rw'         => '01/02',
            'kelurahan'     => 3,
            'tgl_lahir'     => '1995-01-03',
            'jenis_kelamin' => 'L',
        ]);
        $counter++;

        DB::table('pasien')->insert([
            'id_pasien'     => date('ym').'00000'.$counter,
            'nama_pasien'   => 'Budi',
            'alamat'        => 'lorem ipsum',
            'no_telepon'    => '08456',
            'rt_rw'         => '02/03',
            'kelurahan'     => 2,
            'tgl_lahir'     => '1995-02-06',
            'jenis_kelamin' => 'L',
        ]);
        $counter++;

        DB::table('pasien')->insert([
            'id_pasien'     => date('ym').'00000'.$counter,
            'nama_pasien'   => 'Cintia',
            'alamat'        => 'lorem ipsum',
            'no_telepon'    => '08789',
            'rt_rw'         => '03/04',
            'kelurahan'     => 1,
            'tgl_lahir'     => '1995-03-09',
            'jenis_kelamin' => 'P',
        ]);
    }
}
