<?php
namespace App\Repositories;

use App\Interfaces\PasienInterface;
use App\Models\Pasien;

class PasienRepository implements PasienInterface {

    protected $pasien;

    public function __construct(Pasien $pasien) {
        $this->pasien = $pasien;
    }

    public function lastID() {
        $lastid = $this->pasien->orderBy('id_pasien', 'desc')->limit(1)->get();
        return $lastid[0]['id_pasien'];
    }

    public function generateID(string $lastid = null) {
        // return substr($lastid, 0, 4);
        if(substr($lastid, 0, 4) == date('ym')) {
            $counter = (int)substr($lastid, 4) + 1;
            $id = substr($lastid, 0, 4).str_pad($counter, 6, "0", STR_PAD_LEFT);
            return $id;
        }
        $id = date('ym').str_pad(1, 6, "0", STR_PAD_LEFT);
        return $id;
    }

    public function allData() {
        return $this->pasien->select('pasien.*', 'kelurahan.id', 'kelurahan.nama_kelurahan')
                    ->join('kelurahan', 'kelurahan.id', '=', 'pasien.kelurahan')
                    ->orderBy('nama_pasien', 'asc')
                    ->get();
    }

    public function oneData($id) {
        return $this->pasien->select('pasien.*', 'kelurahan.id', 'kelurahan.nama_kelurahan')
                    ->join('kelurahan', 'kelurahan.id', '=', 'pasien.kelurahan')
                    ->where('id_pasien', '=', $id)->get();
    }

    public function store($data) {
        return $this->pasien->create($data);
    }

    public function find($id) {
        return $this->pasien->find($data);
    }

    public function update($data, $id) {
        /*
        $pasien = $this->pasien->where('id', $id)->get();
        $pasien->nama_pasien    = $data['nama_pasien'];
        $pasien->alamat         = $data['alamat'];
        $pasien->no_telepon     = $data['no_telepon'];
        $pasien->rt_rw          = $data['rt_rw'];
        $pasien->kelurahan      = $data['kelurahan'];
        $pasien->tgl_lahir      = $data['tgl_lahir'];
        $pasien->jenis_kelamin  = $data['jenis_kelamin'];
        $pasien->save();
        */
        $res = $this->pasien->where('id_pasien', $id)->update([
            'nama_pasien'    => $data['nama_pasien'],
            'alamat'         => $data['alamat'],
            'no_telepon'     => $data['no_telepon'],
            'rt_rw'          => $data['rt_rw'],
            'kelurahan'      => $data['kelurahan'],
            'tgl_lahir'      => $data['tgl_lahir'],
            'jenis_kelamin'  => $data['jenis_kelamin']
        ]);
        return $res;
    }

    public function delete($id) {
        $res = $this->pasien->where('id_pasien', $id)->delete();
        return $res;
    }

}

?>
