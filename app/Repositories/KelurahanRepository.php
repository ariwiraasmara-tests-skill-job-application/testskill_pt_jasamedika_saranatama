<?php
namespace App\Repositories;

use App\Interfaces\KelurahanInterface;
use App\Models\Kelurahan;

class KelurahanRepository implements KelurahanInterface {

    protected $kelurahan;

    public function __construct(Kelurahan $kelurahan) {
        $this->kelurahan = $kelurahan;
    }

    public function allData() {
        return $this->kelurahan->orderBy('nama_kota', 'asc')->get();
    }

    public function oneData($id) {
        return $this->kelurahan->where('id', '=', $id)->get();
    }

    public function get($name) {
        return $this->kelurahan->select($name)->orderBy('nama_kelurahan', 'asc')->get();
    }

    public function store($data) {
        return $this->kelurahan->create($data);
    }

    public function find($id) {
        return $this->kelurahan->find($id);
    }

    public function updateAll($data, $id) {
        $res = $this->kelurahan->where('id', $id)->update([
            'nama_kelurahan' => $data['nama_kelurahan'],
            'nama_kecamatan' => $data['nama_kecamatan'],
            'nama_kota'      => $data['nama_kota'],
        ]);
        return $res;
    }

    public function updateOne($data, $which, $id) {
        $res = $this->kelurahan->where('id', $id)->update([
            $which => $data[$which],
        ]);
        return $res;
    }

    public function delete($id) {
        $res = $this->kelurahan->where('id', $id)->delete();
        return $res;
    }
}
?>
