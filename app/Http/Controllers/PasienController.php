<?php

namespace App\Http\Controllers;

use App\Repositories\PasienRepository;
use App\Services\PasienService;
use App\Models\Pasien;

use App\Repositories\KelurahanRepository;
use App\Models\Kelurahan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Inertia\Inertia;

class PasienController extends Controller {

    protected $pasienRepository;
    protected $kelurahanRepository;

    public function __construct(PasienRepository $pasienRepository, KelurahanRepository $kelurahanRepository) {
        $this->pasienRepository     = $pasienRepository;
        $this->kelurahanRepository  = $kelurahanRepository;
    }

    public function getData($id = null) {
        $data = $this->pasienRepository->allData();
        if(!is_null($id)) $data = $this->pasienRepository->oneData($id);
        return response()->json(['messages' => 'Data Pasien', 'success' => 1, 'data'=>$data], 200);
    }

    /**
     * Display a listing of the resource.
     */

    public function index() {
        //
        $data = $this->pasienRepository->allData();
        return Inertia::render('pasien/list', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        //
        $data = array('kelurahan' => '');
        // return $data;
        $kelurahan = $this->kelurahanRepository->get(['id', 'nama_kelurahan']);
        return Inertia::render('pasien/create_edit', ['isedit'=>0, 'title'=>'Create Pasien', 'id'=>0, 'data'=>$data, 'kelurahan'=>$kelurahan]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        //
        // $validation = $request->validate([
        //     'nama_pasien'   => 'required',
        //     'alamat'        => 'required',
        //     'no_telepon'    => 'required',
        //     'rt_rw'         => 'required',
        //     'kelurahan'     => 'required',
        //     'tgl_lahir'     => 'required',
        //     'jenis_kelamin' => 'required',
        // ]);
        // $data = $request->all();
        $lastid = $this->pasienRepository->lastID();
        $id = $this->pasienRepository->generateID($lastid);
        $data = array('id_pasien'       => $id,
                      'nama_pasien'     => $request->nama_pasien,
                      'alamat'          => $request->alamat,
                      'no_telepon'      => $request->no_telepon,
                      'rt_rw'           => $request->rt_rw,
                      'kelurahan'       => $request->kelurahan,
                      'tgl_lahir'       => $request->tgl_lahir,
                      'jenis_kelamin'   => $request->jenis_kelamin,
                );

        $res = $this->pasienRepository->store($data);
        if($res) return response()->json(['messages' => 'Berhasil Membuat Data Pasien!', 'success' => 1, 'data'=>$data], 201);
        return response()->json(['messages' => 'Gagal Membuat Data Pasien!', 'success' => 0], 500);
    }

    /**
     * Display the specified resource.
     */
    public function show(Pasien $pasien, $id) {
        //
        $data = $this->pasienRepository->oneData($id);
        $kelurahan = $this->kelurahanRepository->get(['id', 'nama_kelurahan']);
        return Inertia::render('pasien/card', ['title'=>'Kartu Pasien', 'data'=>$data[0], 'kelurahan'=>$kelurahan]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pasien $pasien, $id) {
        //
        $data = $this->pasienRepository->oneData($id);
        $kelurahan = $this->kelurahanRepository->get(['id', 'nama_kelurahan']);
        return Inertia::render('pasien/create_edit', ['isedit'=>1, 'title'=>'Edit Pasien', 'id'=>$id, 'data'=>$data[0], 'kelurahan'=>$kelurahan]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pasien $pasien, $id) {
        //
        /*
       $validation = $request->validate([
            'nama_pasien'   => 'required',
            'alamat'        => 'required',
            'no_telepon'    => 'required',
            'rt_rw'         => 'required',
            'kelurahan'     => 'required',
            'tgl_lahir'     => 'required',
            'jenis_kelamin' => 'required',
        ]);
        */
        $data = $request->all();
        $res = $this->pasienRepository->update($data, $id);
        if($res) return response()->json(['messages' => 'Berhasil Memperbaharui Data Pasien!', 'success' => 1, 'data'=>$data], 200);
        return response()->json(['messages' => 'Gagal Memperbaharui Data Pasien!', 'success' => 0], 500);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pasien $pasien, $id) {
        //
        $res = $this->pasienRepository->delete($id);
        if($res) return response()->json(['messages' => 'Berhasil Menghapus Data Pasien!', 'success' => 1], 200);
        return response()->json(['messages' => 'Gagal Menghapus Data Pasien!', 'success' => 0], 500);
    }
}
