<?php

namespace App\Http\Controllers;

use App\Repositories\KelurahanRepository;
use App\Services\KelurahanService;
use App\Models\Kelurahan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Inertia\Inertia;

class KelurahanController extends Controller {

    protected $kelurahanRepository;

    public function __construct(KelurahanRepository $kelurahanRepository) {
        $this->kelurahanRepository = $kelurahanRepository;
    }

    public function getData($id = null) {
        $data = $this->kelurahanRepository->allData();
        if(!is_null($id)) $data = $this->kelurahanRepository->oneData($id);
        return response()->json(['messages' => 'Data Kelurahan', 'success' => 1, 'data'=>$data], 200);
    }

    /**
     * Display a listing of the resource.
     */
    public function index() {
        //
        $data = $this->kelurahanRepository->allData();
        return Inertia::render('kelurahan/list', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        //
        $data = array();
        return Inertia::render('kelurahan/create_edit', ['isedit'=>0, 'title'=>'Create Kelurahan', 'id'=>0, 'data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        //
        // $validation = $request->validate([
        //     'nama_kelurahan' => 'required',
        //     'nama_kecamatan' => 'required',
        //     'nama_kota'      => 'required'
        // ]);
        $data = $request->all();
        $res = $this->kelurahanRepository->store($data);
        if($res) return response()->json(['messages' => 'Berhasil Membuat Data Kelurahan!', 'success' => 1, 'data'=>$data], 201);
        return response()->json(['messages' => 'Gagal Membuat Data Kelurahan!', 'success' => 0], 500);
    }

    /**
     * Display the specified resource.
     */
    public function show(Kelurahan $kelurahan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kelurahan $kelurahan, $id) {
        //
        $data = $this->kelurahanRepository->oneData($id);
        return Inertia::render('kelurahan/create_edit', ['isedit'=>1, 'title'=>'Edit Kelurahan', 'id'=>$id, 'data'=>$data[0]]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kelurahan $kelurahan, $id) {
        //
        /*
        $validation = $request->validate([
            'nama_kelurahan' => 'required',
            'nama_kecamatan' => 'required',
            'nama_kota'      => 'required'
        ]);
        */
        $data = $request->all();
        $res = $this->kelurahanRepository->updateAll($data, $id);
        if($res) return response()->json(['messages' => 'Berhasil Memperbaharui Data Kelurahan!', 'success' => 1, 'data'=>$data], 200);
        return response()->json(['messages' => 'Gagal Memperbaharui Data Kelurahan!', 'success' => 0], 500);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kelurahan $kelurahan, $id) {
        //
        $res = $this->kelurahanRepository->delete($id);
        if($res) return response()->json(['messages' => 'Berhasil Menghapus Data Kelurahan!', 'success' => 1], 200);
        return response()->json(['messages' => 'Gagal Menghapus Data Kelurahan!', 'success' => 0], 500);
    }
}
