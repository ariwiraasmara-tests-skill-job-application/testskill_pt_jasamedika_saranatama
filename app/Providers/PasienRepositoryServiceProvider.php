<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interface\PasienInterface;
use App\Repositories\PasienRepository;

class PasienRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
        $this->app->bind(PasienInterface::class, PasienRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
