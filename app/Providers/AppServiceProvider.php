<?php

namespace App\Providers;

use App\Models\Kelurahan;
use App\Interface\KelurahanInterface;
use App\Repository\KelurahanRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     */
    public function register(): void {
        //
        $this->app->bind(
            \App\Repositories\KelurahanInterface::class,
            \App\Repositories\KelurahanRepository::class
        );

        // $this->app->bind(
        //     \App\Services\KelurahanServiceInterface::class,
        //     \App\Services\KelurahanService::class
        // );

        $this->app->bind(
            \App\Models\Kelurahan::class,
            function ($app) {
                return new \App\Models\Kelurahan();
            }
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
