<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interface\KelurahanInterface;
use App\Repositories\KelurahanRepository;

class KelurahanRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
        $this->app->bind(KelurahanInterface::class, KelurahanRepository::class);

        $this->app->bind(
            \App\Models\Kelurahan::class,
            function ($app) {
                return new \App\Models\Kelurahan();
            }
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
