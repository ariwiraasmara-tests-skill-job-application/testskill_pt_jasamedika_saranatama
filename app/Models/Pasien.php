<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pasien extends Model {
    use HasFactory;

    protected $guarded = [];
    protected $table = 'pasien';
    protected $primaryKey = 'id_pasien';
    protected $fillable = ['id_pasien',
                            'nama_pasien',
                            'alamat',
                            'no_telepon',
                            'rt_rw',
                            'kelurahan',
                            'tgl_lahir',
                            'jenis_kelamin',];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

}
