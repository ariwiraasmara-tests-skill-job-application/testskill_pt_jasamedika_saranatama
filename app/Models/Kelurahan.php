<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model {
    use HasFactory;

    protected $guarded = [];
    protected $table = 'kelurahan';
    protected $primaryKey = 'id';

    protected $fillable = ['nama_kelurahan',
                            'nama_kecamatan',
                            'nama_kota'];

    protected $attributes = ['nama_kelurahan' => false,
                             'nama_kecamatan' => false,
                             'nama_kota' => false,
                            ];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
