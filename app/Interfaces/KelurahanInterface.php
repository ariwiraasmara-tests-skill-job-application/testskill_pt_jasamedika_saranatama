<?php
namespace App\Interfaces;

use App\Models\Kelurahan;

interface KelurahanInterface {

    public function __construct(Kelurahan $kelurahan);
    public function allData();
    public function oneData($id);
    public function get($name);
    public function store($data);
    public function find($id);
    public function updateAll($data, $id);
    public function updateOne($data, $which, $id);
    public function delete($id);

}
?>
