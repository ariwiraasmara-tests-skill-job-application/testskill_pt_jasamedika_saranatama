<?php

use App\Http\Controllers\PasienController;
use App\Http\Controllers\KelurahanController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
*/

Route::get('/', function(){
    return Inertia::render('home');
})->name('home');

Route::get('/kelurahan', 'App\Http\Controllers\KelurahanController@index')->name('kelurahan');
Route::get('/kelurahan/create', 'App\Http\Controllers\KelurahanController@create')->name('create_kelurahan');
Route::get('/kelurahan/edit/{id}', 'App\Http\Controllers\KelurahanController@edit')->name('edit_kelurahan');

Route::get('/pasien', 'App\Http\Controllers\PasienController@index');
Route::get('/pasien/create', 'App\Http\Controllers\PasienController@create');
Route::get('/pasien/edit/{id}', 'App\Http\Controllers\PasienController@edit');
Route::get('/pasien/card/{id}', 'App\Http\Controllers\PasienController@show');
