<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PasienController;
use App\Http\Controllers\KelurahanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/kelurahan/all', 'App\Http\Controllers\KelurahanController@getData');
Route::get('/kelurahan/{id}', 'App\Http\Controllers\KelurahanController@getData');
Route::post('/kelurahan/save', 'App\Http\Controllers\KelurahanController@store');
Route::post('/kelurahan/update/{id}', 'App\Http\Controllers\KelurahanController@update');
Route::post('/kelurahan/delete/{id}', 'App\Http\Controllers\KelurahanController@destroy');

Route::get('/pasien/all', 'App\Http\Controllers\PasienController@getData');
Route::get('/pasien/{id}', 'App\Http\Controllers\PasienController@getData');
Route::post('/pasien/save', 'App\Http\Controllers\PasienController@store');
Route::post('/pasien/update/{id}', 'App\Http\Controllers\PasienController@update');
Route::post('/pasien/delete/{id}', 'App\Http\Controllers\PasienController@destroy');
