# Deskripsi
Test Skill PT Jasamedika Saranatama

# Cara menggunakan
php artisan serve --port=8789

1. Akses ke http://127.0.0.1:8789/ untuk ke halaman utama (Home Page)
2. Akses ke http://127.0.0.1:8789/kelurahan untuk ke halaman kelurahan 
3. Akses ke http://127.0.0.1:8789/pasien untuk ke halaman pasien

# Powered by
XAMPP
https://www.apachefriends.org/download.html

Composer
https://getcomposer.org/

Laravel
https://laravel.com

Laravel Breeze
https://laravel.com/docs/10.x/starter-kits

VueJS
https://vuejs.org/

Bulma CSS
https://bulma.io/

SweetAlert
https://sweetalert2.github.io/
